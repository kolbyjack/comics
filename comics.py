#!/usr/bin/env python3

try:
    import activate_venv
    activate_venv.activate("venv")
except:
    pass

import datetime
import hashlib
import mimetypes
import os
import re
import requests
import urllib.parse
import yaml

from jinja2 import Environment, FileSystemLoader

def relpath(path):
    basepath = os.path.dirname(os.path.realpath(__file__))
    return os.path.join(basepath, path)

env = Environment(loader=FileSystemLoader(relpath("views")))
template = env.get_template('index.tpl')

#DEBUG_COMICS = ['leasticoulddo']
DEBUG = 'DEBUG_COMICS' in globals() and len(DEBUG_COMICS) > 0

def makedirs(path, perm=0o755):
    try:
        os.makedirs(path, perm)
    except Exception as e:
        pass

def relink(target, name):
    oldtarget = None
    if os.path.islink(name):
        oldtargetpath = os.path.realpath(name)
        oldtarget = os.path.basename(oldtargetpath)
        if target == oldtarget:
            regex = re.compile('<!--yesterday="([^"]*)"-->')
            try:
                with open(oldtargetpath, 'r') as fp:
                    return regex.search(fp.read()).group(1)
            except:
                pass
            return None
        with open(oldtargetpath, 'r+') as fp:
            content = fp.read().replace('<!--nextday-->', ' | <a href="%s">Next day</a>' % target)
            fp.seek(0)
            fp.write(content)
            fp.truncate()
        os.unlink(name)

    os.symlink(target, name)
    return oldtarget

# FIXME: There are more entities
def htmlencode(s):
    entities = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&apos;'
    }
    return ''.join([entities.get(c, c) for c in s])

def search_page(searchurl, pattern, comicdata):
    #print '  searching for %s in %s' % (pattern, searchurl)
    headers = {}
    headers['User-Agent'] = comicdata.get('user-agent', 'dailystrips python clone http://dev.kolbyjack.net/comics/')
    try:
        r = requests.get(searchurl, headers=headers, timeout=60)
    except:
        raise Exception('Error fetching searchpage %s' % searchurl)
    if 200 != r.status_code:
        raise Exception("Invalid status code %s while fetching search page %s" % (r.status_code, searchurl))
    if not r.headers['content-type'].lower().startswith(comicdata.get('content-type', 'text/').lower()):
        raise Exception("Invalid content type %s" % r.headers["content-type"])
    regex = re.compile(pattern, re.IGNORECASE)
    try:
        result = regex.search(r.text).group(1)
    except:
        raise Exception('Unable to find %s in %s' % (pattern, searchurl))
    return urllib.parse.urljoin(comicdata.get('imageurlbase', searchurl), result)

today = datetime.datetime.now()
with open(os.getenv("CONFIG_PATH", relpath("comics.yml")), "r") as stream:
    library = yaml.safe_load(stream)

if 'outdir' in library.setdefault('config', {}):
    outdir = os.path.expandvars(library['config']['outdir'])
else:
    outdir = '%s/out' % os.path.dirname(os.path.realpath(__file__))

basename = today.strftime('%Y-%m-%d')
if DEBUG:
    basename = '%s-DEBUG' % basename

processed = {}
for group, groupdata in library['groups'].items():
    for comic in groupdata['comics']:
        if comic in processed:
            continue

        try:
            print('Processing %s' % comic)
            data = {'definition': library['comics'].get(comic, {})}

            if DEBUG and comic not in DEBUG_COMICS:
                raise Exception('Skipped for debugging')

            if comic not in library['comics']:
                raise Exception('Unknown comic')
            comicdata = library['comics'][comic]

            if 'generate' in comicdata:
                imageurl = today.strftime(comicdata['generate'])
            elif 'searchpattern' in comicdata:
                if 'prefetchpattern' in comicdata:
                    prefetchurl = today.strftime(comicdata.get('prefetchpage', comicdata['homepage']))
                    searchurl = search_page(prefetchurl, comicdata['prefetchpattern'], comicdata)
                else:
                    searchurl = today.strftime(comicdata.get('searchpage', comicdata['homepage']))
                imageurl = search_page(searchurl, comicdata['searchpattern'], comicdata)
            else:
                raise Exception('Unknown comic type')

            data['url'] = imageurl

            try:
                r = requests.get(imageurl, timeout=60)
                if 200 != r.status_code:
                    raise Exception
            except Exception:
                raise Exception('Unable to fetch image URL %s (%d)' % (imageurl, r.status_code))

            url = urllib.parse.urlparse(imageurl)
            name, ext = os.path.splitext(url.path)
            if 0 == len(ext):
                mime = r.headers['content-type'].lower().split(';', 1)[0]
                ext = mimetypes.guess_extension(mime, strict=False) or '.img'

            comicdir = 'strips/%s' % library['comics'][comic]['name']
            makedirs('%s/%s' % (outdir, comicdir))
            data['file'] = '%s/%s%s' % (comicdir, hashlib.sha256(r.content).hexdigest(), ext)
            filename = '%s/%s' % (outdir, data['file'])
            if not os.path.exists(filename):
                with open(filename, 'wb') as image:
                    image.write(r.content)
            linkname = '%s/%s/%s%s' % (outdir, comicdir, basename, ext)
            if os.path.exists(linkname):
                os.unlink(linkname)
            os.link(filename, linkname)

        except Exception as e:
            print('  error: %s' % e)
            data['error'] = htmlencode(str(e))

        processed[comic] = data

for group, groupdata in library['groups'].items():
    makedirs('%s/%s' % (outdir, group))

    if not DEBUG:
        oldtarget = relink('%s.html' % basename, '%s/%s/index.html' % (outdir, group))
    else:
        oldtarget = None

    with open('%s/%s/%s.html' % (outdir, group, basename), 'w') as page:
        page.write(template.render(comics=groupdata['comics'], comicdata=processed,
            today=today.strftime('%A, %B %d, %Y'), now=today.strftime('%H:%M on %a, %d %b %Y'),
            yesterday=oldtarget))

